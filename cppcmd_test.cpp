#include "cppcmd.h"
#include <iostream>

int main()
{
	std::string ret;

	// input output pipe test -------------------------------------------
	std::cout << "//================================" << std::endl;
	std::cout << "// run cmd test" << std::endl;
	if (1) {
		std::cout << "// sort b c d a" << std::endl;
		ret = cppcmd::run_cmd(".", "sort", "b\r\nc\r\nd\r\na\r\n");
		std::cout << ret << std::endl;

		std::cout << "//--------------------------------" << std::endl;
		std::cout << "// pwd" << std::endl;
		ret = cppcmd::run_cmd(".", "pwd");
		std::cout << ret << std::endl;

		std::cout << "//--------------------------------" << std::endl;
		std::cout << "// ls -la" << std::endl;
		ret = cppcmd::run_cmd(".", "ls -la");
		std::cout << ret << std::endl;

		//	compiler::run_cmd("wc", "aa aa aa");	// wc bug...
		//	std::cout << ret << std::endl;
	}

	std::cout << "//================================" << std::endl;
	std::cout << "// pwd" << std::endl;
	std::cout << cppcmd::pwd() << std::endl;
	std::cout << std::endl;

	std::cout << "//================================" << std::endl;
	std::cout << "// mkdir test" << std::endl;
	std::cout << cppcmd::mkdir("test") << std::endl;
	std::cout << std::endl;

	std::cout << "//================================" << std::endl;
	std::cout << "// ls" << std::endl;
	auto fl = cppcmd::ls(".");
	for (auto i : fl) {
		std::cout << i.name << std::endl;
	}
	std::cout << std::endl;

	std::cout << "//================================" << std::endl;
	std::cout << "// if exist test" << std::endl;
	bool f = cppcmd::if_exist("test");
	std::cout << "1==" << f << std::endl;
	std::cout << std::endl;

	std::cout << "//================================" << std::endl;
	std::cout << "// cd test" << std::endl;
	cppcmd::cd("test");
	std::cout << cppcmd::pwd() << std::endl;
	cppcmd::cd("..");
	std::cout << cppcmd::pwd() << std::endl;
	std::cout << std::endl;

	std::cout << std::endl;
	return 0;
}
