//
//	cppcmd.h
//
#pragma once

#include <stdio.h>
#include <string>
#include <vector>
#include <stdint.h>
namespace cppcmd{
	struct ls_data {
		std::string path;
		std::string name;
		uint64_t size;
		time_t time_create;
		time_t time_write;
		time_t time_access;
		bool read_only;
		bool hidden;
		bool directory;
	};

	std::string run_cmd(const std::string& dir, const std::string& cmd);
	std::string run_cmd(const std::string& dir, const std::string& cmd, const std::string& istr);
	std::string pwd();
	bool cd(const std::string& path);
	bool mkdir(const std::string& path, bool recursive = true);
	std::vector<ls_data> ls(std::string dir);

	bool if_exist(const std::string& path);

}// namespace cppcmd
