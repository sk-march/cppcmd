//
//	cppcmd.cpp
//
#define __FORCE_MESSAGE 0
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <string.h>
#include <functional>
#include <iostream>
#include <fstream>

#include "cppcmd.h"
#if defined( _MSC_VER) || defined(__MINGW32__)
#include <windows.h>
#pragma warning(disable:4996)
#else
#include <sys/stat.h>
#include <dirent.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <unistd.h>
#define _putenv putenv
#endif

#if defined(__MINGW32__) && !defined(_MSC_VER)
#include <unistd.h>
#define _putenv putenv
#endif

namespace cppcmd {

std::string run_cmd(const std::string& dir, const std::string& cmd)
{
	return run_cmd(dir, cmd, "");
}

std::string run_cmd(const std::string& dir, const std::string& cmd, const std::string& istr)
{
	if(__FORCE_MESSAGE) std::cout << "cmd[" << cmd << "]" << std::endl;

	fflush(stdout);
	std::string ostr;
#if defined( _MSC_VER) || defined(__MINGW32__)
	BOOL ret;
	HANDLE hOutputReadTmp, hOutputRead, hOutputWrite;
	HANDLE hInputWriteTmp, hInputRead, hInputWrite;
	HANDLE hErrorWrite;
	SECURITY_ATTRIBUTES sa;

	// Set up the security attributes struct.
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	// Create the child output pipe.
	CreatePipe(&hOutputReadTmp, &hOutputWrite, &sa, 0);
	DuplicateHandle(GetCurrentProcess(), hOutputWrite, GetCurrentProcess(), &hErrorWrite, 0, TRUE, DUPLICATE_SAME_ACCESS);

	// Create the child input pipe.
	CreatePipe(&hInputRead, &hInputWriteTmp, &sa, 0);
	DuplicateHandle(GetCurrentProcess(), hOutputReadTmp, GetCurrentProcess(), &hOutputRead, 0, FALSE, DUPLICATE_SAME_ACCESS);
	DuplicateHandle(GetCurrentProcess(), hInputWriteTmp, GetCurrentProcess(), &hInputWrite, 0, FALSE, DUPLICATE_SAME_ACCESS);
	
	CloseHandle(hOutputReadTmp);
	CloseHandle(hInputWriteTmp);

	// create process
	DWORD creationFlags = 0;// CREATE_NEW_CONSOLE;// 0;

	STARTUPINFOA si = {};
	si.cb = sizeof(STARTUPINFOA);
	si.dwFlags = STARTF_USESTDHANDLES;
	si.hStdOutput = hOutputWrite;
	si.hStdInput = hInputRead;
	si.hStdError = hErrorWrite;

	// run cmd
	PROCESS_INFORMATION pi = {};
	CreateProcessA(
		NULL,
		(LPSTR)cmd.c_str(),
		NULL,	//process security
		NULL,	//thread security
		TRUE,
		creationFlags,
		NULL,	//environments inherits
		dir.c_str(),	//working directory inherits
		&si,
		&pi);
	HANDLE childProcess = pi.hProcess;
	CloseHandle(pi.hThread);

	// close handle
	CloseHandle(hOutputWrite);
	CloseHandle(hInputRead);
	CloseHandle(hErrorWrite);

	// write istr
	DWORD numberOfBytes;
	ret = WriteFile(hInputWrite, istr.c_str(), static_cast<DWORD>(istr.size()), &numberOfBytes, NULL);
	if(!ret) return "";
	CloseHandle(hInputWrite);

	// read ostr
	char t[256];
	while (ReadFile(hOutputRead, t, sizeof(t)-1, &numberOfBytes, NULL) && numberOfBytes!=0) {
		t[numberOfBytes]='\0';
		ostr += t;
	}
	CloseHandle(hOutputRead);

	// wait close child
	DWORD r = WaitForSingleObject(childProcess, INFINITE);


	return ostr;
#else
	int pid;
	int pipe_c2p[2], pipe_p2c[2];
	const int R = 0;
	const int W = 1;

	/* Create two of pipes. */
	if (pipe(pipe_c2p)<0) {
		perror("popen1");
		return "";
	}
	if (pipe(pipe_p2c)<0) {
		perror("popen2");
		close(pipe_c2p[R]);
		close(pipe_c2p[W]);
		return "";
	}

	/* Invoke processs */
	if ((pid = fork())<0) {
		perror("popen3");
		close(pipe_c2p[R]);
		close(pipe_c2p[W]);
		close(pipe_p2c[R]);
		close(pipe_p2c[W]);
		return "";
	}

	if (pid == 0) { /* I'm child */
		close(pipe_p2c[W]);
		close(pipe_c2p[R]);
		dup2(pipe_p2c[R], 0);
		dup2(pipe_c2p[W], 1);
		close(pipe_p2c[R]);
		close(pipe_c2p[W]);
		cd(dir.c_str());
		if (execlp("sh", "sh", "-c", cmd.c_str(), NULL)<0) {
			perror("popen4");
			close(pipe_p2c[R]);
			close(pipe_c2p[W]);
			exit(1);
		}
	}

	close(pipe_p2c[R]);
	close(pipe_c2p[W]);

	// write pipe input
	ssize_t size = 0;
	while(size < istr.size()){
		size += write(pipe_p2c[W], &istr.c_str()[size], istr.size()-size);
	}
	close(pipe_p2c[W]);

	// read result
	char t;
    while(true){
        size=read(pipe_c2p[R], &t, 1);
        if(size==0) break;
        ostr += t;
    }
	close(pipe_c2p[R]);

	return ostr;
#endif
}

std::string pwd()
{
#if defined(_MSC_VER)
	std::string ret;
	ret.resize(256);
	uint32_t s = GetCurrentDirectoryA(256, &ret[0]);
	ret.resize(s);
	return ret;
#else
	char ret[1024];
	if (getcwd(ret, 1024) == NULL) {
		perror("getcwd() error");
		return "";
	}
	else {
		return ret;
	}
#endif
}

bool cd(const std::string& path)
{
#if defined(_MSC_VER)
	BOOL ret = SetCurrentDirectoryA(&path[0]);
	return ret == TRUE;
#else
	if (chdir(path.c_str())!=0) {
		perror("chdir() error");
		return false;
	}
	else {
		return true;
	}
#endif

}

bool if_exist(const std::string& path)
{
	struct stat st;
	int result;
	result = stat(path.c_str(), &st);
	return result == 0;
}

bool mkdir(const std::string& path, bool recursive)
{
#ifdef _MSC_VER
	bool f = TRUE==CreateDirectoryA(path.c_str(), NULL);
#else
	mode_t mode = 0755;
	std::string tn = path;
	std::string::size_type l = tn.find('\\');
	while (l != tn.npos) {
		tn[l] = '/';
		l = tn.find('\\');
	}
    bool f = !(::mkdir(tn.c_str(), mode));
#endif
	if (!f && recursive) {
		std::string::size_type p = path.rfind("/");
		if (p != path.npos) {
			f = mkdir(path.substr(0, p), true);
			f = mkdir(path, false);
		}
	}
	return f;
}

#if defined(_MSC_VER)
// http://yoshi2soft.tobiiro.jp/tips/windows_time.html
time_t FileTimeToUnixTime(const FILETIME& ft) {
	long long int ll;
	ll = ((long long int)ft.dwHighDateTime << 32) + ft.dwLowDateTime;
	return (time_t)((ll - 116444736000000000) / 10000000);
}

void ls_add(const char* dir, std::vector<ls_data>& in, WIN32_FIND_DATAA& fil)
{
	ls_data ld;
	ld.path = dir;
	ld.path += "/";
	ld.path += fil.cFileName;
	ld.name = fil.cFileName;
	ld.size = fil.nFileSizeLow;
	ld.size += ((uint64_t)fil.nFileSizeHigh) << 32;
	/*
	ld.time_create	 = fil.ftCreationTime.dwLowDateTime;
	ld.time_create	+= ((uint64_t)fil.ftCreationTime.dwHighDateTime) << 32;
	ld.time_write	 = fil.ftLastWriteTime.dwLowDateTime;
	ld.time_write	+= ((uint64_t)fil.ftLastWriteTime.dwHighDateTime) << 32;
	ld.time_access	 = fil.ftLastAccessTime.dwLowDateTime;
	ld.time_access	+= ((uint64_t)fil.ftLastAccessTime.dwHighDateTime) << 32;
	*/
	ld.time_create = FileTimeToUnixTime(fil.ftCreationTime);
	ld.time_write = FileTimeToUnixTime(fil.ftLastWriteTime);
	ld.time_access = FileTimeToUnixTime(fil.ftLastAccessTime);

	ld.read_only = (fil.dwFileAttributes & FILE_ATTRIBUTE_READONLY);
	ld.hidden = (fil.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != 0;
	ld.directory = (fil.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0;
	in.push_back(ld);
}
#else
void ls_add(const char* dir, std::vector<ls_data>& in, dirent *dp)
{
	ls_data ld;
	ld.path = dir;
	ld.path += "/";
	ld.path += dp->d_name;
	ld.name = dp->d_name;

	struct stat st;
	stat(ld.path.c_str(), &st);

	ld.size = st.st_size;

	ld.time_create = st.st_ctime;
	ld.time_write = st.st_mtime;
	ld.time_access = st.st_atime;

	ld.read_only = access(ld.path.c_str(), W_OK) != 0;
	ld.hidden = (dp->d_name[0] == '.') && ((dp->d_type&DT_DIR) == 0);
	ld.directory = (dp->d_type&DT_DIR) != 0;
	in.push_back(ld);
}
#endif

std::vector<ls_data> ls(std::string dir)
{
	std::vector<ls_data> lds;
	{
		// file
#if defined(_MSC_VER)
		WIN32_FIND_DATAA fil;
		HANDLE ret;

		std::string dir2 = dir + "\\*.*";
		if ((ret = FindFirstFileA(dir2.c_str(), &fil)) != INVALID_HANDLE_VALUE) {
			ls_add(dir.c_str(), lds, fil);
			while (true) {
				if (!FindNextFileA(ret, &fil)) {
					if (GetLastError() == ERROR_NO_MORE_FILES) break;
					else {
						break;
					}
				}
				ls_add(dir.c_str(), lds, fil);
			}
			//		uint32_t a = GetLastError();
			FindClose(ret);
		}
#else
		DIR *dir_mat;
		dirent *dp;
		dir_mat = opendir(dir.c_str());
		if (dir_mat != NULL) {
			for (dp = readdir(dir_mat); dp != NULL; dp = readdir(dir_mat)) {
				ls_add(dir.c_str(), lds, dp);
			}
			closedir(dir_mat);
		}
#endif
	}
	return lds;
}

}	// namespace
